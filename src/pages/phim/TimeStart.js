import React, { useMemo } from 'react'
import {   Tabs  } from "antd";
import 'antd/dist/antd.css';
import "./TimeStart.css"
import { selectDesktopData } from "./selectData"
import formatDate from './FormateDate';
import ItemCumRap from './ItemCumRap';
import Times from './Times';
export default function TimeStart({currentSelectedHeThongRapChieu}) {
    const desktopData = useMemo(
      () => selectDesktopData(currentSelectedHeThongRapChieu),
      [currentSelectedHeThongRapChieu]
    );
    console.log(currentSelectedHeThongRapChieu)
    // console.log(desktopData.allArrayCumRapChieuFilterByDay)
  return (
    
    <div>
      <Tabs
        defaultActiveKey="0"
        tabPosition="top"
        style={{
          marginBottom: 8,
          textSize:22,
          height:550
        }}
        items={desktopData.arrayDay.map((day, i) => {
          const id = String(i);
          return {
            label: 
            <div>
              <p className='mb-0 text-[14px]'>{formatDate(day).dayToday}</p>
              <p className='mb-0 text-[14px]'>{formatDate(day).YyMmDd}</p>
            </div>,
            key: id,
            children: 
              <div>
                {currentSelectedHeThongRapChieu.cumRapChieu.map((cumrap,index)=>(
                  <div key={index}>
                    <div className='flex space-x-3 mt-3'>
                      <img className='w-[60px] h-18' src={cumrap.hinhAnh} alt=""/>
                      <div>
                        <p className='text-[16px] font-semibold mb-1'>{cumrap.tenCumRap}</p>
                        <p>{cumrap.diaChi}</p>
                      </div>
                    </div>
                    <div className='flex'>
                      <Times lichChieuPhim={cumrap.lichChieuPhim}/>
                    </div>
                  </div>
                ))}
              </div>
            
          };
        })}
      />
    </div>
  )
}