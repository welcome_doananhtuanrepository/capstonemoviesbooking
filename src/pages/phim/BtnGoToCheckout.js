import React from 'react'
// import {useNavigate} from "react-router-dom"
export default function BtnGoToCheckout({ lichChieuTheoPhim }) {
  // const navigate=useNavigate();

  const calculateTimeout = (ngayChieuGioChieu) => {
    const fakeThoiLuong = 120
    const timeInObj = new Date(ngayChieuGioChieu);
    const timeOutObj = new Date(timeInObj.getTime() + fakeThoiLuong * 60 * 1000);
    return timeOutObj.toLocaleTimeString([], { hour12: false }).slice(0, 5)
  }
  // const handleBookingTicket=()=>{
  //   navigate(`/datve/${lichChieuTheoPhim.maLichChieu}`, `/datve/${lichChieuTheoPhim.maLichChieu}`)
  // }
  return (
    <button className='border p-1 bg-gray-300 rounded-md mb-2 mr-2'>
      <span>{lichChieuTheoPhim.ngayChieuGioChieu.slice(11, 16)}</span>
      <span>{` ~ ${calculateTimeout(lichChieuTheoPhim.ngayChieuGioChieu)}`}</span>
    </button>
  )
}
