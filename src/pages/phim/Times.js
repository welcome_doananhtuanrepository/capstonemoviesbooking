import React from "react";

export default function Times({ lichChieuPhim }) {
    // console.log(lichChieuPhim)
  const groupByDate = (data) => {
    // this gives an object with dates as keys
    const groups = data.reduce((groups, khungGioChieu) => {
        // console.log(groups)
      const ngayChieu = khungGioChieu.ngayChieuGioChieu.split("T")[0];
    //   console.log(ngayChieu)
      const gioChieu = khungGioChieu.ngayChieuGioChieu.split("T")[1];
    //   console.log(gioChieu)
        console.log(groups)
      if (!groups[ngayChieu]) {
        groups[ngayChieu] = [];
      }
      groups[ngayChieu].push({...khungGioChieu, gioChieu: gioChieu});
      return groups;

    }, {});

    // Edit: to add it in the array format instead
    const groupArrays = Object.keys(groups).map((ngayChieu) => {
      return {
        ngayChieu,
        cacKhungGioChieu: groups[ngayChieu],
      };
    });
    return groupArrays;
  };
//   groupByDate(lichChieuPhim)
  
  return (
    <div  className="flex space-x-3 mt-4 text-[16px] items-center flex-wrap">
        <p className="font-semibold mb-0">Suất chiếu:</p>
         {groupByDate(lichChieuPhim)[0].cacKhungGioChieu.map((item,index)=>(
        <button className="bg-gray-300 rounded-md m-2" key={index}>
            <p className="mb-0 px-1 py-1">{item.gioChieu} </p>
        </button>
      ))
      }
    </div>
  );
}
