import React, { useEffect, useState } from 'react'  
import {  Tabs  } from "antd";
import 'antd/dist/antd.css';
import TimeStart from './TimeStart';
export default function LichChieu({heThongRapChieu}) {
  console.log(heThongRapChieu)
  return (
    <div className='border my-6 text-[white] bg-white rounded-lg'>
        <Tabs
        defaultActiveKey="0"
        tabPosition="left"
        className='md:w-[800px] sm:w-[600px] w-[450px] h-[550px]'
        items={heThongRapChieu.map((movie, id) => {
         
          return {
            label:
            <div className='flex items-center space-x-2'>
              <img src={movie.logo} alt="" className="w-[50px]"/>
              <span className='text-[16px]'>{movie.maHeThongRap}</span>
            </div>,
            key: `${movie.maHeThongRap}`,
            children: 
              <div>
                {heThongRapChieu?.length === 0 && <p>Hiện tại chưa có lịch chiếu cho phim này</p>}
                  <div key={movie.maHeThongRap}>
                    <TimeStart currentSelectedHeThongRapChieu={movie} />
                  </div>
              </div>
          };
        })}
      />
    </div>
  )
}

