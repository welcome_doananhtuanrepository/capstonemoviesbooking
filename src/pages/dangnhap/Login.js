import React, { useEffect, useState } from "react"; 
import logo from "../../../src/images/logo.svg";
import {AiFillEye, AiOutlineCloseCircle} from "react-icons/ai"
import {Navigate, useLocation, useNavigate} from "react-router-dom"
import { useSelector, useDispatch } from "react-redux";
import * as yup from "yup";
import { ErrorMessage, Field, Form, Formik } from "formik";
import { login, resetErrorLoginRegister } from "../../redux/action/Auth";
import Loading from "../loading/Loading";
const Login = () => {
  const [showPass,setShowPass]=useState(true)
    let location = useLocation();
    const {currentUser,errorLogin}=useSelector(state=>state.authReducer)
    console.log(currentUser,errorLogin)
    const dispatch = useDispatch();
    const navigate=useNavigate();
    const handleNavigate=()=>{
        navigate("/")
    }
    const signinUserSchema = yup.object().shape({
      taiKhoan: yup.string().required("*Tài khoản không được bỏ trống !"),
      matKhau: yup.string().required("*Mật khẩu không được bỏ trống !"),
    });
    const handleSubmit = (user) => {
        dispatch(login(user));
      };
    const handleDangKy=()=>{
      navigate("/dangky")
    }
    useEffect(()=>{
      if(currentUser){
        if(location.state==="/"){
          setTimeout(() => {
            navigate("/")
          }, 50);
          return undefined; 
        }
      }
    },[currentUser])
    useEffect(()=>{
      dispatch(resetErrorLoginRegister())
    },[])
    return ( 
        <div className="w-full h-screen">
            <div className="w-full bg-cover bg-hero4 bg-center flex justify-center">
            <div className="w-[full] h-screen"></div>
            <div className="w-[600px] h-[480px] rounded-md login relative my-auto">
                <div className="mt-6">
                    <img src={logo} alt="logo" className="cursor-pointer mx-auto" />
                </div>
                <p className="text-white text-center mt-4">Thế giới phim trên đầu ngón tay</p>
                <p className="text-white text-center mt-5 text-[18px]">Đăng Ký để được nhiều ưu đãi, mua vé và bảo mật thông tin!</p>
                <Formik
                    initialValues={{
                      taiKhoan: "",
                      matKhau: "",
                    }}
                    validationSchema={signinUserSchema}
                    onSubmit={handleSubmit}
                  >
              {() => (
                <Form className="col-sm-10 mx-auto px-8">
                  <div className="form-group position-relative">
                    <label>Tài khoản&nbsp;</label>
                    <ErrorMessage
                      name="taiKhoan"
                      render={(msg) => <small className="text-red-500">{msg}</small>}
                    />
                    <Field type="text" className="form-control" name="taiKhoan" />
                  </div>

                  <div className="form-group relative">
                    <label>Mật khẩu&nbsp;</label>
                    <ErrorMessage
                      name="matKhau"
                      render={(msg) => <small className="text-red-500">{msg}</small>}
                    />
                    <Field
                      type={`${showPass?"password":"text"}`}
                      className="form-control "
                      name="matKhau"
                    />
                    <AiFillEye onClick={()=>setShowPass(!showPass)} className="cursor-pointer absolute top-[35px] right-[10px] text-black"/>
                    
                  </div>
                  <p
                    className="text-green-600 text-[18px]"
                    style={{ cursor: "pointer" }}
                    onClick={handleDangKy}
                  >
                    * Đăng ký
                  </p>
                  <button
                    type="submit"
                    className="block mx-auto px-4 py-2 bg-slate-500 rounded-lg text-white"
                  >
                    Đăng nhập
                  </button>
                  {/* error from api */}
                  {errorLogin && (
                    <div className="alert alert-danger mt-3">
                      <span> {errorLogin}</span>
                    </div>
                  )}
                </Form>
              )}
            </Formik>
                    <div className="text-white absolute top-0 right-0 cursor-pointer" onClick={handleNavigate}>
                            <AiOutlineCloseCircle size={"30px"}/>
                    </div>
                </div>
                
                </div>
            </div>
        
     );
}
 
export default Login;