import React, { useState,useEffect } from "react"; 
import logo from "../../../src/images/logo.svg";
import { useForm } from "react-hook-form";
import {AiOutlineCloseCircle} from "react-icons/ai"
import {useNavigate} from "react-router-dom"
import {registerUser,resetErrorLoginRegister} from "../../redux/action/Auth";
import { useSelector, useDispatch } from "react-redux";
import * as yup from "yup";
import { ErrorMessage, Field, Form, Formik } from "formik";
import Swal from "sweetalert2";
const SignIn = () => {
    // const {register,getValues , formState: { errors },} = useForm();
    const dispatch=useDispatch()
    const navigate=useNavigate();
    const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
    const signupUserSchema = yup.object().shape({
        taiKhoan: yup.string().required("*Tài khoản không được bỏ trống !"),
        matKhau: yup.string().required("*Mật khẩu không được bỏ trống !"),
        email: yup
          .string()
          .required("*Email không được bỏ trống !")
          .email("* Email không hợp lệ "),
        soDt: yup
          .string()
          .required("*Số điện thoại không được bỏ trống !")
          .matches(phoneRegExp, "Số điện thoại không hợp lệ!"),
        hoTen: yup.string().required("*Tên không được bỏ trống !"),
      });
      
      
    const { responseRegister, loadingRegister, errorRegister } = useSelector(state=> state.authReducer);

    const handleNavigate=()=>{
        navigate("/")
    }
    const handleSubmit = (user) => {
        // trường hợp nào thì cho đăng ký(return true): loadingRegister=false và responseRegister=null
        console.log(`user`, user);
        if (!loadingRegister && !responseRegister) {
          dispatch(registerUser(user));
        }
      };
    
    useEffect(() => {
        if (responseRegister) {
          // đăng ký thành công thì đăng nhập, responseRegister để bỏ qua componentditmount
          Swal.fire({
            position: "center",
            icon: "success",
            title: "Bạn đã đăng ký thành công",
            showConfirmButton: false,
            timer: 2000,
          });
          navigate("/dangnhap")
        }
      }, [responseRegister]);
      useEffect(() => {
        return () => {
          dispatch(resetErrorLoginRegister());
        };
      }, []);
    return ( 
        <div className="w-full h-full bg-cover bg-hero4 bg-center flex pt-10 justify-center">
            <div className="signin relative">
                <div className="mt-6">
                    <img src={logo} alt="logo" className="cursor-pointer mx-auto" />
                </div>
                <p className="text-white text-center mt-4">Thế giới phim trên đầu ngón tay</p>
                <p className="text-white text-center mt-5 text-[18px]">Đăng Ký để được nhiều ưu đãi, mua vé và bảo mật thông tin!</p>
                <Formik
        initialValues={{
          taiKhoan: "",
          matKhau: "",
          email: "",
          soDt: "",
          maNhom: "GP09",
          maLoaiNguoiDung: "KhachHang", // điền QuanTri backend cũng áp dụng KhachHang
          hoTen: "",
        }}
        validationSchema={signupUserSchema} // validationSchdema:  thu vien yup nhập sai ko submit được
        onSubmit={handleSubmit}
      >
        {(formikProps) => (
          <Form className="col-sm-12 px-8 mt-6">
            <div className="form-group">
              <label>Tài khoản&nbsp;</label>
              <ErrorMessage
                name="taiKhoan"
                render={(msg) => <span className="text-red-500">{msg}</span>}
              />
              <Field name="taiKhoan" type="text" className="form-control" />
            </div>
            <div className="form-group">
              <label>Mật khẩu&nbsp;</label>
              <ErrorMessage
                name="matKhau"
                render={(msg) => <span className="text-red-500">{msg}</span>}
              />
              <Field name="matKhau" type="password" className="form-control" />
            </div>
            <div className="form-group">
              <label>Họ và tên&nbsp;</label>
              <ErrorMessage
                name="hoTen"
                render={(msg) => <span className="text-red-500">{msg}</span>}
              />
              <Field name="hoTen" type="text" className="form-control" />
            </div>

            <div className="form-group">
              <label>Email&nbsp;</label>
              <ErrorMessage
                name="email"
                render={(msg) => <span className="text-red-500">{msg}</span>}
              />
              <Field name="email" type="email" className="form-control" />
            </div>
            <div className="form-group">
              <label>Số điện thoại&nbsp;</label>
              <ErrorMessage
                name="soDt"
                render={(msg) => <span className="text-red-500">{msg}</span>}
              />
              <Field name="soDt" type="text" className="form-control" />
            </div>
            <div className="text-center p-2">
              <button
                type="submit"
                className="px-6 py-2 text-[18px] rounded-md bg-blue-500"
                disable={loadingRegister.toString()}
              >
                Đăng Ký
              </button>
              {/* error from api */}
              {errorRegister && (
                <div className="text-red-500 mt-5 text-[18px]">
                  <span>{errorRegister}</span>
                </div>
              )}
            </div>
          </Form>
        )}
      </Formik>
                <div className="text-white absolute top-0 right-0 cursor-pointer" onClick={handleNavigate}>
                        <AiOutlineCloseCircle size={"30px"}/>
                </div>
            </div>
            
        </div>
     );
}
 
export default SignIn;