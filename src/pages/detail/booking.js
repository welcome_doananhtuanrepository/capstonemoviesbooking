import 'antd/dist/antd.css';
import { Divider, Steps,Input, Radio, Space } from 'antd';
import React, { useState } from 'react';
import "./booking.css"
const { Step } = Steps;

export default function Booking() {
    const [current, setCurrent] = useState(0);
    const [checkout,setCheckout]=useState(true)
    const [value, setValue] = useState(0);

    const onChangeRadio = (e) => {
        setValue(e.target.value);
    };
    const onChange = (value) => {
        setCurrent(value);
      };
  return (
    <div className='h-full w-full flex'>
        <div className='bg-white w-[70%]'>
            <div className='h-[100px] p-4 flex justify-between shadow-3xl px-6'>
                <div className="lg:w-[750px] md:w-[680px] w-[400px] flex items-center">
                    <Steps current={current} onChange={onChange}>
                        <Step title="CHỌN GHẾ" />
                        <Step title="THANH TOÁN" />
                        <Step title="KẾT QUẢ ĐẶT VÉ" />
                    </Steps>
                </div>
                <div className="flex items-center justify-center">
                    <div className='flex flex-col items-center space-y-2'>
                        <div><img className='w-[40px] h-[40px] rounded-[50%]' src="https://znews-photo.zingcdn.me/w360/Uploaded/qfssu/2022_10_24/312711135_665529898471061_6854198162972919707_n_1.jpg" alt=""/></div>
                        <h1 className='mb-0'>ANH TUAN 18</h1>
                    </div>
                </div>
            </div>
        </div>
        <div className='w-[30%] h-full'>
            <div className='w-[100%] h-[600px] bg-slate-200 shadow-3xl px-6'>
                <h1 className='text-center text-[#44c020] text-[45px] pt-[24px] mb-0'>0đ</h1>
                <Divider className='my-[12px]'/>
                <div>
                    <h1>Rừng săn người</h1>
                    <p>BHD Star Cineplex - 3/2</p>
                    <p>Thứ bảy 30/7/2022- 11:07-Rạp 1</p>
                </div>
                <Divider/>
                <h1 className=''>Ghế: </h1>
                <Divider/>
                <label>E-Mail</label>
                <p>doantuanmin94@gmail.com</p>
                <Divider/>
                <label>Phone</label>
                <p>0906240120</p>
                <Divider/>
                <label>Hình thức thanh toán</label>
                {checkout&&<p className='text-red-500'>Vui lòng chọn ghế để hiển thị phương thức thanh toán phù hợp</p>}
                {checkout&&
                <Radio.Group onChange={onChangeRadio} value={value}>
                <Space direction="vertical">
                  <Radio value={1}>Option A</Radio>
                  <Radio value={2}>Option B</Radio>
                  <Radio value={3}>Option C</Radio>
                
                </Space>
              </Radio.Group>}
            </div>
        </div>
        
    </div>
  )
}
