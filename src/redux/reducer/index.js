import { combineReducers } from "redux";
import authReducer from "./AuthReducer"
import movieReducer from "./MoviesReducer"
import theaterReducer from "./theaterReducer"
// import movieReducer from "./Movie";
// import usersManagementReducer from "./UsersManagement";
// import theaterReducer from "./Theater";
// import bookTicketReducer from "./BookTicket";
// import movieDetailReducer from "./MovieDetail";
// import modalTrailerReducer from "./ModalTrailer";
// import lazyReducer from "./Lazy";

const rootReducer = combineReducers({
  authReducer,movieReducer,theaterReducer
});
export default rootReducer;