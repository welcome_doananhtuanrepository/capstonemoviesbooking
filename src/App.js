import { Suspense, lazy } from "react";

import {BrowserRouter,Routes,Route} from "react-router-dom";

import Loading from "./pages/loading/Loading";

const Home = lazy(() => import("./components/Home/Home"));
const SignIn = lazy(() => import("./pages/dangky/Signin"));
const Login = lazy(() => import("./pages/dangnhap/Login"));
const Phim = lazy(() => import("./pages/phim/Phim"));
const Booking= lazy(() => import("./pages/detail/booking"));

function App() {
  return (
    <div className="font-main">
      <BrowserRouter>
        <Suspense fallback={<Loading />}>
          <Routes>
              <Route path="/" element={<Home/>}></Route>
              <Route path="/dangky" element={<SignIn/>}></Route>
              <Route path="/dangnhap" element={<Login/>}></Route>
              <Route path="/phim/:maPhim" element={<Phim/>}></Route>
              <Route path="/booking" element={<Booking/>}></Route>
          </Routes>
        </Suspense>
      </BrowserRouter>
      
    </div>
  );
}

export default App;
