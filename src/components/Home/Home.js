import React, { useEffect, useState } from "react";
import ComingSoon from "./ComingSoon";
import Contact from "./Contact";
import Footer from "./Footer";
import Header from "./Header";
import MoviesNew from "./MoviesNew";
import HeaderMobile from "./HeaderMobile/HeaderMobile"
import Nav from "./Nav/Nav";
import SearchTicket from "./SearchTicket/SearchTicket";
import ListMovie from "./ListMovie/ListMovie";
import { useDispatch, useSelector } from "react-redux";
import { getMovieList } from "../../redux/action/Movies";
import { getTheaters } from "../../redux/action/Theater"
import Theater from "./Theater/Theater";
function Home() {
  const [isShow, setIsShow] = useState(false);

  const dispatch = useDispatch();
  const movieList = useSelector((state) => state.movieReducer.movieList);
  const theaterList = useSelector((state) => state.theaterReducer.theaterList);

  useEffect(() => {
    getMovieList()
      if (!movieList.length){
        dispatch(getMovieList());
      }
      if (!theaterList.length){
      dispatch(getTheaters());
      }
  }, []);
  return (
    <div className="flex flex-col items-center relative overflow-hidden">
      <Nav isShow={isShow} setIsShow={setIsShow}/>
      <HeaderMobile isShow={isShow} setIsShow={setIsShow}/>
      <Header setIsShow={setIsShow} />
      <ListMovie/>
      <Theater/>
      {/* <MoviesNew /> */}
      <ComingSoon />
      <Contact />      
      <Footer />    
    </div>
  );
}

export default Home;
