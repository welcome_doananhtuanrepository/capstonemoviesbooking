import { GrFormNext } from "react-icons/gr";
import { FaUserCircle } from "react-icons/fa";

const HeaderMobile=({isShow,setIsShow})=>{
    return(
        <>
       <div
        className={`absolute md:hidden top-0 left-0 h-screen bg-slate-700 z-[999999] w-full min-h-full opacity-[0.5] ${
          isShow ? "block" : "hidden"
        } overflow-hidden`}
        onClick={() => setIsShow(false)}
      ></div>
      <div
        className={`w-[320px] translate-x-[100%] min-h-full bg-slate-400 md:hidden absolute right-0 z-[999999999] ${
          isShow ? "translate-x-[0%] " : "translate-x-[100%]"
        } `}
      >
        <div className="flex m-[40px] items-center">
          <span className="text-[24px] text-[#999]">
            <FaUserCircle />
          </span>
          <span className="ml-[30px] text-[24px] text-[#666] cursor-pointer">Đăng nhập</span>
          <span className="flex ml-[60px] text-[25px] cursor-pointer" onClick={()=>setIsShow(false)}>
            <GrFormNext size={"25px"}/>
          </span>
        </div>
        <div className="py-[20px] px-[40px] hover:bg-slate-200 hover:text-[#ec7532] cursor-pointer mb-[10px]">
          <span className="text-[20px] text-[#666] ">Lịch chiếu</span>
        </div>
        <div className="py-[20px] px-[40px] hover:bg-slate-200 hover:text-[#ec7532] cursor-pointer mb-[10px]">
          <span className="text-[20px] text-[#666] ">Cụm rạp</span>
        </div>
        <div className="py-[20px] px-[40px] hover:bg-slate-200 hover:text-[#ec7532] cursor-pointer mb-[10px]">
          <span className="text-[20px] text-[#666] ">Tin tức</span>
        </div>
        <div className="py-[20px] px-[40px] hover:bg-slate-200 hover:text-[#ec7532] cursor-pointer mb-[10px]">
          <span className="text-[20px] text-[#666] ">Ứng dụng</span>
        </div>
        <div className="py-[20px] px-[40px] hover:bg-slate-200 hover:text-[#ec7532] cursor-pointer mb-[10px]">
          <span className="text-[20px] text-[#666] ">Đăng ký</span>
        </div>
      </div></>
    )
}

export default HeaderMobile