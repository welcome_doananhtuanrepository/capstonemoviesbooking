import React, { Fragment } from 'react'
import BtnGoToCheckout from './BtnGoToCheckout';
import formatDate from './FormateDate';

export default function LstNgayChieu({lstLichChieuTheoPhim}) {
    const mangChiChuaNgay = lstLichChieuTheoPhim.map(item => {  // tạo mảng mới chỉ chứa ngày
        return item.ngayChieuGioChieu.slice(0, 10);// item là "2020-12-17" cắt ra từ 2020-12-17T10:10:00
    })
    const MangNgayKhongTrungLap = [...new Set(mangChiChuaNgay)] // xóa đi ngày trùng lặp > dùng mảng này để render số phần tử
    const filterByDay = (date) => { // lọc ra item từ mảng gốc
        const gioChieuRenDer = lstLichChieuTheoPhim.filter(item => {
          if (item.ngayChieuGioChieu.slice(0, 10) === date) {
            return true
          }
          return false
        })
        return gioChieuRenDer;
      }
    
    return (
        <div>
        {MangNgayKhongTrungLap.map(date => (
          <Fragment key={date}>
            <p>{formatDate(date).dateFull}</p> {/*in ra ngày hiện tại*/}
            <div>
              {filterByDay(date).map(lichChieuTheoPhim => (
                <Fragment key={lichChieuTheoPhim.maLichChieu}>
                  <BtnGoToCheckout lichChieuTheoPhim={lichChieuTheoPhim} />
                </Fragment>
              ))}
            </div>
  
          </Fragment>
        ))
        }
      </div >
  )
}
