import React from 'react'
import LstNgayChieu from './LstNgayChieu'


export default function LstPhim({lstPhim}) {
  return (
    <div className='h-[650px] overflow-auto'>
      {lstPhim.map(phim => (
        <div key={phim.maPhim}>
          <div className='flex'>
            <img className='w-[60px] h-[60px] object-cover' src={phim.hinhAnh} alt={phim.tenPhim} />
            <div className='ml-2'>
              <p className='text-[17px] mb-[5px] font-semibold'>{phim.tenPhim}</p>
              <p>120 phút- Điểm Tix 10</p> {/* phải tách riêng ra vì thời lượng và đánh giá lấy từ một api khác */}
            </div>
          </div>
          <div>{/* div danh sách ngày giờ chiếu */}
            <LstNgayChieu lstLichChieuTheoPhim={phim.lstLichChieuTheoPhim} />
          </div>
        </div>
      ))}
    </div>
  )
}
