import axios from "axios"
import React, { useEffect, useState } from 'react'
import { useSelector } from "react-redux";
import {  Radio, Tabs  } from "antd";
import 'antd/dist/antd.css';
import LstCumRap from "./LstCumRap";
export default function Theater() {
  const [theaterList,setTheaterList]=useState([])
  // const { theaterList } = useSelector(
  //   (state) => state.theaterReducer
  // );
  useEffect(() => {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP04",
      method: "GET",
      headers: {
        TokenCybersoft:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzM0UiLCJIZXRIYW5TdHJpbmciOiIxOS8wNC8yMDIzIiwiSGV0SGFuVGltZSI6IjE2ODE4NjI0MDAwMDAiLCJuYmYiOjE2NTQzNjIwMDAsImV4cCI6MTY4MjAxMDAwMH0.8vVBHKZZpOpTUa6ep4mWe7SQc5U-y_8IFYOnVCJLEgI",
      },
    })
      .then((res) => {
        setTheaterList(res.data.content)
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="h-[650px] w-[80%] mx-auto border mt-10">
      <Tabs
        defaultActiveKey="0"
        tabPosition="left"
        style={{
          height: 650,
        }}
        items={theaterList.map((theater, id) => {
          return {
            label:<img src={theater.logo} alt="" className="w-[30px]"></img>,
            key: `${theater.maHeThongRap}`,
            children: 
              <LstCumRap
              lstCumRap={theater.lstCumRap}
              maHeThongRap={theater.maHeThongRap}
              />,
          };
        })}
      />
    </div>
  )
}
