import React from 'react'

import {   Tabs  } from "antd";
import 'antd/dist/antd.css';
import LstPhim from './LstPhim';
export default function LstCumRap({lstCumRap}) {
  return (
    <div>
        <Tabs
            defaultActiveKey="0"
            tabPosition="left"
            style={{
            height: 650,
            }}
            items={lstCumRap.map((cumRap, id) => {
            
            return {
                label:  <div className='text-left  w-[400px]'>
                            <p>
                                <span>{cumRap.tenCumRap?.split("-")[0]}</span>
                                <span>-{cumRap.tenCumRap?.split("-")[1]}</span>
                            </p>
                            <p>{cumRap.diaChi}</p>
                        </div>,
                key: `${cumRap.maCumRap}`,
                children: 
                    <LstPhim
                    lstPhim={cumRap.danhSachPhim} 
                    key={cumRap.maCumRap} 
                    />
            };
            })}
  /></div>
  )
}
