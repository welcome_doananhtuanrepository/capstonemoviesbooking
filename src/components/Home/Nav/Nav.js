import React from "react";
import { AiOutlineUserAdd } from "react-icons/ai";
import { FaBars } from "react-icons/fa";
import { FiLogIn } from "react-icons/fi";
import {Link} from "react-router-dom"
import logo from "../../../images/logo.svg";
const Nav = ({isShow,setIsShow}) => {
    const navLinks = ["Trang chủ", "Lịch chiếu", "Cụm rạp", "Tin tức", "Ứng dụng"];
    return ( 
        <div className="w-[100%] bg-gradient-to-r from-black to-orange-700">
            <div className="my-0 mx-auto p-2 max-w-[90%] ">
            <div className="w-full flex justify-end items-center text-[white] font-bold text-[16px]">
                <span className="">
                    <FiLogIn/>
                </span>
                <Link to="/dangnhap">
                    <span className="mx-[20px] cursor-pointer">
                        Đăng nhập <span className="mx-[5px]"></span>|
                    </span>
                </Link>
                    <span className="">
                        <AiOutlineUserAdd />
                    </span>
                
                
                <Link to="/dangky">
                    <span className="mx-[20px] cursor-pointer">
                        Đăng ký <span className="mx-[5px]"></span>|
                    </span>
                </Link>
            </div>
            <div className="flex items-center justify-between">
                <img src={logo} alt="logo" className="cursor-pointer" />
                <div
                    className="flex md:hidden text-[#fbbd61] text-2xl cursor-pointer z-20"
                    onClick={() => setIsShow((prev) => !prev)}
                >
                    <FaBars />
            </div>
          
            <div className="md:flex hidden">
                <ul className="flex items-center border-b border-white/20 ml-8">
                    {navLinks.map((link) => (
                        <li
                        key={link}
                        className={`link px-5 py-4 uppercase text-[white] text-sm font-bold lg:px-8 lg:py-6 lg:text-base cursor-pointer ${
                            link === "Trang chủ" ? "active" : ""
                        }`}
                        >
                        {link}
                        </li>
                    ))}
                    </ul>
                </div>
            </div>
        </div>
        </div>
        
     );
}
 
export default Nav;

